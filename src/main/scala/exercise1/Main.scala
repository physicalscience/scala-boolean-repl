package main.scala.exercise1

/**
  * Created by blanthrip on 9/14/2016.
  */
object Main extends App {
  val c = new Constants
  while(true) {
    print("# ")
    try {
      val p = new Parser(new LexicalAnalyzer, c)
      p.parse()
    } catch {
      case e: ParserException => println("There was an error parsing")
    }
}

}
