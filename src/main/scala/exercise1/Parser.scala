package main.scala.exercise1

import java.util.NoSuchElementException

import scala.collection.mutable

/**
  * Created by blanthrip on 9/9/2016.
  */
class Parser(l: LexicalAnalyzer, c: Constants){
  val lex = l
  val lexList = c.lexList
  val map = c.map
  var introId = ' '
  var notCounter = 0
  var operandStack = new scala.collection.mutable.Queue[Int]()
  var operandLiteralStack = new scala.collection.mutable.Queue[Char]()

  def parse() = {
    s()
  }

  def s() = {
      a()
      if(!lex.matcher(lex.end).equals(0)) {

      } else
        throw new ParserException
  }

  def a() = {
    val id = lex.matcher(lex.lit)
    if(!id.equals(0)) {
        introId = id.asInstanceOf[Char]
        b()
      }

  }

  def b() = {
    if(!lex.matcher('=').equals(0)) {
      e()
    }
    else if(!lex.matcher('?').equals(0)) {
      map.get(introId) match {
        case Some(i) => println(i)
        case None => throw new ParserException
      }
    } else {
      throw new ParserException
    }
  }

  def e(): Unit = {
    t()
    e2()
  }

  def e2(): Unit = {
    while(!lex.matcher('^').equals(0)) {
      e()
      val op = operandStack.last
      operandStack.get(operandStack.length - 2) match {
        case Some(i) =>
          var j = i
          println("this is i: " + i)
          j ^= op
          operandStack.update(operandStack.length - 2, j)
          map.update(introId, j)
        case None => throw new ParserException
      }
      e2()
    }
    while(!lex.matcher('|').equals(0)) {
      e()
      var op = operandStack.last
      operandStack.get(operandStack.length - 2) match {
        case Some(i) =>
          var j = i
          j |= op
          operandStack.update(operandStack.length - 2, j)
          map.update(introId, j)
        case None => throw new ParserException
      }
      e2()
    }
    while(!lex.matcher('&').equals(0)) {
      e()
      var op = operandStack.last
      operandStack.get(operandStack.length - 2) match {
        case Some(i) =>
          var j = i
          j &= op
          operandStack.update(operandStack.length - 2, j)
          map.update(introId, j)
        case None => throw new ParserException
      }

      e2()
    }
  }

  def t() = {
    if(!lex.matcher('~').equals(0)) {
      e()
      if(operandStack.last.equals(0)) {
        operandStack.update(operandStack.size - 1, 1)
      }else {
        operandStack.update(operandStack.size - 1, 0)
      }
    }else if(!lex.matcher('(').equals(0)) {
      e()
      if(!lex.matcher(')').equals(0)) {
      } else {
        throw new ParserException
      }
    } else {
      val id = lex.matcher(lex.lit)
      if (!id.equals(0)) {
        map.get(id.asInstanceOf[Char]) match {
          case Some(i) => operandStack.enqueue(i)
          case None => throw new ParserException
        }
        operandLiteralStack.enqueue(id.asInstanceOf[Char])
      }
      val op = lex.matcher(lex.bool)
      if (!op.equals(0)) {
        if (op.asInstanceOf[Char].equals('0')) {
          operandStack.enqueue(0)
          map.update(introId, 0)
        } else if (op.asInstanceOf[Char].equals('1')) {
          operandStack.enqueue(1)
          map.update(introId, 1)
        } else {
          throw new ParserException
        }
      }
    }
  }
}
