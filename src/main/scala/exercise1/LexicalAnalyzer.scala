package main.scala.exercise1

import scala.collection.mutable

/**
  * Created by blanthrip on 9/14/2016.
  */
class LexicalAnalyzer{
  val end = '$'
  val lit = 'L'
  val bool = 'B'
  var yyText = ' '
  var tkn = ' '

  this.next()

  def next(): Unit = {
    if(!tkn.equals(end)) {
      val c = System.in.read()
      yyText = c.toChar

      if(c.equals(-1)) {
        yyText = end
        tkn = end
      }

        yyText match {
          case '\n' =>
            yyText = end
            tkn = end
          case '=' => tkn = yyText
          case '?' => tkn = yyText
          case '&' => tkn = yyText
          case '|' => tkn = yyText
          case '^' => tkn = yyText
          case '~' => tkn = yyText
          case '(' => tkn = yyText
          case ')' => tkn = yyText
          case literal if yyText >= 'a' && yyText <= 'z' => tkn = lit
          case boole if yyText >= '0' && yyText <= '1' => tkn = bool
          case rec if yyText.equals(' ') => next()
          case _ =>
            print("Wrong Character " + yyText + "\n")
            next()
        }
      }
    }

  def check(tok: Char):Int = {
    if(tok.equals(tkn)) {
      yyText
    }
    else {
      0
    }
  }

  def matcher(tok: Char): Int = {
    val lexVal = check(tok)
    if(!lexVal.equals(0)) {
      next()
    }
    lexVal
  }
}
